import socket
from scapy.all import *


class UserPacketFilter:
    def __init__(self, packet, anomaly):
        self.packet = packet
        self.anomaly = anomaly

    def name(self):
        if not IP in self.packet:
            return False

        _ip = (
            self.packet[IP].src if self.anomaly["whr"] == "src" else self.packet[IP].dst
        )

        try:
            addr_name = socket.gethostbyaddr(_ip)
        except:
            return False
        if self.anomaly["f"] == "eq":
            return addr_name[0].endswith(self.anomaly["args"][0])

        elif self.anomaly["f"] == "ne":
            return not addr_name[0].endswith(self.anomaly["args"][0])

        elif self.anomaly["key"] == "Ip":
            if not IP in self.packet:
                return False
            _ip = (
                self.packet[IP].src
                if self.anomaly["whr"] == "src"
                else self.packet[IP].dst
            )
            if self.anomaly["f"] == "eq":
                return _ip == self.anomaly["args"][0]
            elif self.anomaly["f"] == "ne":
                return _ip != self.anomaly["args"][0]

    def ip(self):

        if not IP in self.packet:
            return False
        _ip = (
            self.packet[IP].src if self.anomaly["whr"] == "src" else self.packet[IP].dst
        )
        if self.anomaly["f"] == "eq":
            return _ip == self.anomaly["args"][0]
        elif self.anomaly["f"] == "ne":
            return _ip != self.anomaly["args"][0]

    def filter(self):

        if self.anomaly["key"] == "Name":
            return self.name()

        elif self.anomaly["key"] == "Ip":
            return self.ip()
