from scapy.all import rdpcap
from user_filter import UserPacketFilter


class WPackets:
    def __init__(self, file=None, l=None):
        if l == None:
            self._plist = list(rdpcap(file))
        else:
            self._plist = l

    def __getitem__(self, item):
        return self._plist[item]

    def __len__(self):
        return len(self._plist)

    def detect(self, *args):

        return WPackets(
            l=list(filter(lambda x: KeyWordFilter.and_filter(x, *args), self._plist))
        )


class KeyWordFilter:
    """
    Custom operators parse
    """

    # 'And' keyword filter
    @staticmethod
    def and_filter(packet, *args):
        return all(KeyWordFilter._filter(packet, arg) for arg in args)

    # 'Or' keyword filter
    @staticmethod
    def or_filter(packet, *args):
        return any(KeyWordFilter._filter(packet, arg) for arg in args)

    @staticmethod
    def _filter(packet, anomaly):
        if isinstance(anomaly, Expr):
            if anomaly.operator == "And":
                return KeyWordFilter.and_filter(packet, *anomaly.args)

            elif anomaly.operator == "Or":
                return KeyWordFilter.or_filter(packet, *anomaly.args)

        else:
            return UserPacketFilter(packet, anomaly).filter()


class AnomalyRef:
    """
    Packet expressions
    """

    def __init__(self, key, args=None):
        if args is None:
            args = {}
        self.key = key
        self.args = args

    def __ne__(self, other):
        self.args.update({"f": "ne", "args": [other], "key": self.key})
        return self.args

    def __eq__(self, other):
        self.args.update({"f": "eq", "args": [other], "key": self.key})
        return self.args


class Expr:
    """
    Or
    """

    def __init__(self, operator, *args):
        self.operator = operator
        self.args = args

    def __repr__(self):
        return str(self.operator) + str(list(self.args))


def Or(*args):
    """
    Create a w3 or-expression
    """
    return Expr("Or", *args)


def And(*args):
    """
    Create a w3 and-expression
    """
    return Expr("And", *args)


def Ip(whr):
    """
    Create a w3 ip
    """
    return AnomalyRef("Ip", {"whr": whr})


def Name(whr):
    """
    Create a w3 name
    """
    return AnomalyRef("Name", {"whr": whr})
