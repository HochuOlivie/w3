from w3 import *
import socket
from scapy.all import IP as S_IP

# Примеры использования
name_src = Name("src")
ip_src = Ip("src")
name_dst = Name("dst")
ip_dst = Ip("dst")

wp = WPackets("vk-traffic.pcapng")

# wp.detect возвращает объект типа WPackets,
# то есть мы и к нему можем применить какой-нибудь фильтр.
# Если не указать никакого оператора ('Or', 'And'), будет
# использован And
anomalies = wp.detect(
    name_src == "vk.com",
    Or(
        ip_src == "228",
        ip_src == "87.240.129.131",
        ip_src == "87.240.129.186",
    ),
    ip_src != "87.240.129.131",
)
# Напечатаю все эти пакеты, чтобы убедиться в том, что
# все имена получателей -- vk.com, а ip -- ip == 87.240.129.186
# for i in anomalies:
#     print(f"{socket.gethostbyaddr(i[S_IP].ip_src)[0]}:{i[S_IP].ip_src}")
print(len(anomalies))

# Очередной пример
anomalies = wp.detect(
    Or(
        And(name_src == "vk.com", ip_src == "87.240.129.183"),
        And(name_src == "vk.com", ip_src == "87.240.129.131"),
    )
)
print(len(anomalies))

# Вк сам себе очевидно ничего не посылает через нас
anomalies = wp.detect(name_src == "vk.com", name_dst == "vk.com")
assert len(anomalies) == 0
